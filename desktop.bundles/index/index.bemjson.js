module.exports = {
    block: 'page',
    title: 'Deviserweb Ltd.',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: 'Описание проекта' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'index.min.css' },
        { elem: 'css', url: 'https://fonts.googleapis.com/css?family=Open+Sans&display=swap'},
        // { elem: 'meta', attrs: { property='og:title', content: 'Заголовок' } },
        // { elem: 'meta', attrs: { property='og:description', content: 'Описание' } },
        // { elem: 'meta', attrs: { property='og:image', content: 'адрес картинки, полный' } },
        // { elem: 'meta', attrs: { property='og:type', content: 'website' } },
        // { elem: 'meta', attrs: { property='og:url', content: 'https://site.org/' } },
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],
    mods: { theme: 'light' },
    content: [
        // { block: 'trafaret' },
        { block: 'header' },
        { block: 'working-process' },
        { block: 'featured-works' },
        { block: 'services-provide' },
        { block: 'team-skills' },
        { block: 'clients-said' },
        { block: 'footer'}
    ]
};
