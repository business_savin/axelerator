module.exports = {
    block: 'page',
    title: 'Deviserweb Ltd.',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'index.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],
    mods: { theme: 'islands' },
    content: [
        { block: 'content',
            content: [
                { tag: 'h2', content: 'Test content' },
                {
                    block: 'menu-nav'
                }
            ]
        },
        {
					block: 'slider',
					content: [
						{
							elem: 'item',
							content: {
								elem: 'img',
								url: '../../img/1.jpg'

							}
						},
						{
							elem: 'item',
							content: {
								elem: 'img',
								url: '../../img/2.jpg'
							}
						},
						{
							elem: 'item',
							content: {
								elem: 'img',
								url: '../../img/3.jpg'
							}
						},
						{
							elem: 'item',
							content: {
								elem: 'img',
								url: '../../img/4.jpg'
							}
						},
						{
							elem: 'item',
							content: {
								elem: 'img',
								url: '../../img/5.jpg'
							}
						}
					]
				}
    ]
};
