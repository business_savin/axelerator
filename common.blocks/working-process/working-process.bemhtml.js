block('working-process').content()(function () {
    return [{
      elem: 'wrapp',
      content: [{
        block: 'title',
        mods: { align: 'center', 'text-size': 'h1'},
        content: {
            html: '<h2><span>Working</span> Process</h2>'
          }
        },
        {
          block: 'text',
          tag: 'p',
          mods: {cont: 'middle', size:'m'},
          content: 'Lorem ipsum dolor sit amet consectet ur adipiscing elit  Vestibulum bibend um vestibulum.'
        },
        {
          elem: 'cards',
          content:[
            {
              elem: 'card',
              elemMods: {color: 'blue'},
              content: [
                { block: 'icon', mods: { size: 'm', color: 'green', glyph: 'magnet'} },
                { block: 'title', mods: { size: 'x', color: 'white' }, content: 'Koc Koi Tomi'},
                { block: 'text', mods: {padding: 'card', color: 'white'}, content: 'Lorem ipsum is dummy lorem very dumy'},
                { elem: 'wrapp-button', content:

                  { block: 'button',
                    mods: { position: 'middle', theme: 'snow' },
                    content: [
                      {
                        block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'plus'}
                      },
                      {
                        block: 'text',
                        mods: { color: 'black', padding: 'left', size:'ml' },
                        tag: 'span',
                        content:'Know More'
                      }
                    ]
                  }

                }
              ]
            },
            {
              elem: 'card',
              elemMods: {color: 'folet'},
              content:  [
                { block: 'icon', mods: { size: 'm', color: 'green', glyph: 'bug'} },
                { block: 'title', mods: { size: 'x', color: 'white' }, content: 'Koimona Set'},
                { block: 'text', mods: {padding: 'card', color: 'white'},  content: 'Lorem ipsum is dummy you   lorem very dumyL is '},
                { elem: 'wrapp-button', content:

                  { block: 'button',
                    mods: { position: 'middle', theme: 'snow' },
                    content: [
                      {
                        block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'plus'}
                      },
                      {
                        block: 'text',
                        mods: { color: 'black', padding: 'left', size:'ml' },
                        tag: 'span',
                        content:'Know More'
                      }
                    ]
                  }

                }
              ]
            },
            {
              elem: 'card',
              elemMods: {color: 'siren'},
              content:  [
                { block: 'icon', mods: { size: 'm', color: 'green', glyph: 'gears'} },
                { block: 'title', mods: { size: 'x', color: 'white' }, content: 'Setting Koire'},
                { block: 'text', mods: {padding: 'card', color: 'white'},  content: 'Lorem ipsum is dummy lorem very yLorem ipsus '},
                { elem: 'wrapp-button', content:

                  { block: 'button',
                    mods: { position: 'middle', theme: 'snow' },
                    content: [
                      {
                        block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'plus'}
                      },
                      {
                        block: 'text',
                        mods: { color: 'black', padding: 'left', size:'ml' },
                        tag: 'span',
                        content:'Know More'
                      }
                    ]
                  }

                }
              ]
            },
            {
              elem: 'card',
              elemMods: {color: 'pink'},
              content:  [
                { block: 'icon', mods: { size: 'm', color: 'green', glyph: 'camera-retro', transform: 'rotate'} },
                { block: 'title', mods: { size: 'x', color: 'white' }, content: 'Ko Image Lara'},
                { block: 'text', mods: {padding: 'card', color: 'white' },  content: 'Lorem ipsum is dummy ry dumyLorem ipsum is '},
                { elem: 'wrapp-button', content:

                  { block: 'button',
                    mods: { position: 'middle', theme: 'snow' },
                    content: [
                      {
                        block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'plus'}
                      },
                      {
                        block: 'text',
                        mods: { color: 'black', padding: 'left', size:'ml' },
                        tag: 'span',
                        content:'Know More'
                      }
                    ]
                  }

                }
              ]
            }
        ]
      }]
    }];
  });
