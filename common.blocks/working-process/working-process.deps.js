({
  shouldDeps: [
    { block: 'logo' },
    { block: 'text' },
    { block: 'title' },
    { block: 'clear' },
    { block: 'icon', mods: { glyph: 'gears' } },
    { block: 'icon', mods: { glyph: 'magnet' } },
    { block: 'icon', mods: { glyph: 'bug' } },
    { block: 'icon', mods: { glyph: 'camera-retro' } },
    { block: 'icon', mods: { glyph: 'plus' } },
    { block: 'dropdown' },
    { block: 'popup' },
    { block: 'button' },
    { block: 'form' }

  ]
})
