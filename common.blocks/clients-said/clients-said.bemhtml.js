block('clients-said').content()(function () {
    return [{
      elem: 'wrapp',
      content: [{
        block: 'title',
        mods: { align: 'center', 'text-size': 'h1' },
        content: {
            html: '<h2><span>Clients</span> Said</h2>'
          }
        },
        {
          block: 'text',
          tag: 'p',
          mods: { cont: 'middle', size:'m', align: 'center'},
          content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit Maiores fuga provident Libero.'
        },
        { block: 'container',
          content:
          {
            block: 'slider',
            mods: { 'height-size': 'm' },
  					content: [
      						{
      							elem: 'item',
      							content: {
      								elem: 'img',
      								url: '../../img/1.jpg'
      							}
      						},
      						{
      							elem: 'item',
      							content: {
      								elem: 'img',
      								url: '../../img/2.jpg'
      							}
      						},
      						{
      							elem: 'item',
      							content: {
      								elem: 'img',
      								url: '../../img/3.jpg'
      							}
      						},
      						{
      							elem: 'item',
      							content: {
      								elem: 'img',
      								url: '../../img/4.jpg'
      							}
      						},
      						{
      							elem: 'item',
      							content: {
      								elem: 'img',
      								url: '../../img/5.jpg'
      							}
      						}
      					]
    		   }
        }
      ]
      }]
});
