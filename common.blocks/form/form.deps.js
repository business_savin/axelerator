({
  shouldDeps: [
    { block: 'text' },
    { block: 'title' },
    { block: 'icon', mods: { glyph: 'arrow-right' } },
    { block: 'textarea' },
    { block: 'button' },
    { block: 'input' }
  ]
})
