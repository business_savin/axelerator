block('form').content()(function() {
  return [{
    elem: 'wrapp',
      content:[{
        block: 'title',
        mods: { align: 'center' },
        content: {
            html: '<h2><span>Send</span> A Message</h2>'
          }
      },
      {
        block: 'input',
        mods: {
          theme: 'light',
          size: 'm',
          type: 'text',
          width: '100'
        },
        placeholder: 'Your Name'
      },
      {
        block: 'input',
        mods: {
          theme: 'light',
          size: 'm',
          type: 'test',
          width: '100'
        },
        placeholder: 'Your E-mail'
      },
      {
          block: 'textarea',
          mods: {
              theme: 'light',
              size: 'm',
              padding: 'top'
          },
          placeholder: 'Your Message'
      },
      {
        block: 'button',
        mods: { position: 'middle', theme: 'sunny' },
        content: [{
            block: 'text',
            mods: { color: 'white', padding: 'right'},
            tag: 'span',
            content:'Send Message'
          },
          {
            block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'arrow-right'}
        }]
      }
    ]
  }]
});
