block('header').content()(function() {
  return [{
    elem: 'top',
      content:[
      {
        block: 'logo',
        mods: { position: 'left'},
      },
      {
        block: 'icon',
        mods: { glyph: 'navicon', size: 'menu', position: 'right' }
      },
      {
        block: 'clear',
        mods: { width: '100' }
      },
      {
          block: 'dropdown',
          mods: {
              switcher: 'link',
              theme: 'islands',
              size: 'm'
          },
          switcher: 'Узнать об акциях на сайте',
          popup: 'Скидка 30% на новую коллекцию. Для активации акции нужно ввести промокод.'
      }
    ]
    },
    { elem: 'middle',
      content: [{
          block: 'title',
          content: {
              html: '<h1><span>Smart</span> Agency Landign Page</h1>'
            }
        },
        {
          block: 'text',
          tag: 'p',
          mods: {cont: 'middle', size:'m'},
          content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, fuga provident. Libero magni quibusdam labore ad iste saepe hic, maxime qui ipsum tempore beatae, volu tomi ptates doloremque rerum aspernatur dolorum at!'
        }
      ]
    },
    {
      elem: 'bottom',
      content: [{block: 'form'}]
    }]
});
