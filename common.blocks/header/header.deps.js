({
  shouldDeps: [
    { block: 'logo' },
    { block: 'text' },
    { block: 'title' },
    { block: 'clear' },
    { block: 'icon', mods: { glyph: 'navicon' } },
    { block: 'dropdown' },
    { block: 'popup' },
    { block: 'button' },
    { block: 'form' }
  ]
})
