block('logo').content()(function() {
  return [{
    block: 'image',
    url: '../../img/logo.jpg',
    title: 'Логотип компании',
    width: 145,
    height: 55
  }]
});
