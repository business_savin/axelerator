block('icons-soc').content()(function() {
  return [{
    elem: 'content',
    content: [
      { block: 'link',
        url: 'https://dribbble.com/',
        target:'blank',
        mods: { padding: 'horizontal', block: 'round', padding: 'size-s', margin: 'size-s'},
        content: { block: 'icon', mods: { size: 'xs', color: 'green', glyph: 'dribbble'} }
      },
      { block: 'link',
        url: 'https://facebook.com/',
        target:'blank',
        mods: { padding: 'horizontal', block: 'round', padding: 'size-s', margin: 'size-s'},
        content: { block: 'icon', mods: { size: 'xs', color: 'green', glyph: 'facebook'} }
      },
      { block: 'link',
        url: 'https://vk.com/',
        target:'blank',
        mods: { padding: 'horizontal', block: 'round', padding: 'size-s', margin: 'size-s'},
        content: { block: 'icon', mods: { size: 'xs', color: 'green', glyph: 'vk'} }
      },
      { block: 'link',
        url: 'https://twitter.com/',
        target:'blank',
        mods: { padding: 'horizontal', block: 'round', padding: 'size-s', margin: 'size-s'},
        content: {
          block: 'icon',
           mods: { size: 'xs', color: 'green', glyph: 'twitter' }
        }
      },
      {
        block: 'copyright',
        mods: { padding: 'top', padding: 'size-s', padding: 'left-size-s' }
      }]
  }]
});
