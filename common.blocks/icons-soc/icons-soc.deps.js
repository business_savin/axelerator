({
  shouldDeps:[
    { block: 'icon', mods: { glyph: 'facebook-official' } },
    { block: 'icon', mods: { glyph: 'facebook' } },
    { block: 'icon', mods: { glyph: 'dribbble' } },
    { block: 'icon', mods: { glyph: 'vk'} },
    { block: 'icon', mods: { glyph: 'twitter'} },
    { block: 'copyright' }
   ]
})
