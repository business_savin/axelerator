block('footer').content()(function () {
    return [
      {
        elem: 'header',
        elemMods: { padding: 'space' },
        content: [{
          block: 'button',
          mods: { position: 'middle', theme: 'snow', size:'xl' },
          content: 'Contact Us'
        }]
      },
      {
      elem: 'content',
      elemMods: { padding: 'horizontal', padding:'bottom'},
      content: [{
        block: 'icons-soc',
        content: 'должны выводится иконки из блока'
      }
    ]
    }];
  });
