block('parnters').content()(function () {
    return [{
      elem: 'content',
      elemMods: {},
      content: [
        {
          block: 'row',
          content: [
            {
              elem: 'col',
              elemMods: { mw: 6 },
              content: 'left column'
            },
            {
              elem: 'col',
              elemMods: { mw: 6 },
              content: 'right column'
            }
          ]
        }
      ]
    }];
  });
