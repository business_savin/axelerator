({
  shouldDeps: [
    { block: 'text' },
    { block: 'title' },
    { block: 'clear' },
    { block: 'icon', mods: { glyph: 'twetter' } },
    { block: 'icon', mods: { glyph: 'facebook' } },
    { block: 'progressbar' },
    { block: 'button' },
    { block: 'row' },
    { block: 'ul' },
    { block: 'li' }
  ]
})
