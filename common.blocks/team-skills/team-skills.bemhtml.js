block('team-skills').content()(function () {
    return [{
      elem: 'wrapp',
      content: [{
        block: 'title',
        mods: { align: 'center', 'text-size': 'h1'},
        content: {
            html: '<h2><span>Team</span> Chinen & Skills</h2>'
          }
        },
        {
          block: 'text',
          tag: 'p',
          mods: { cont: 'middle', size:'m', align: 'center'},
          content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit Maiores fuga provident Libero.'
        },
        {
          elem: 'items',
          content:[
            {
              elem: 'item',
              elemMods: { background: 'orange', display: 'line'},
              content: {
                elem: 'info-blok',
                content: [
                  {
                    block: 'title',
                    mods: {size: 'ml'},
                    content: 'Bedi Manos'
                  },
                  {
                    block: 'text',
                    mods: { size: 's', padding: 'all-10' },
                    content: 'UI/UX Parena'
                  }
                ]
              }
            },
            {
              elem: 'item',
              elemMods: { display: 'line' },
              content:
                {
                  elem: 'item-wrapp',
                  content: [
                    { block: 'text', mods: { size:'xs' }, content:'UI/UX  Design'},
                    { block: 'progressbar', mods: { theme: 'light' }, val: 80 },
                    { block: 'text', mods: { size:'xs' }, content:'English'},
                    { block: 'progressbar', mods: { theme: 'light' }, val: 70 },
                    { block: 'text', mods: { size:'xs' }, content:'Communication'},
                    { block: 'progressbar', mods: { theme: 'light' }, val: 65 },
                    { block: 'title', mods: { size: 'ml', padding: 'top-57' }, content: 'Al Rayhan' },
                    { block: 'text', mods: { size: 's', color: 'orange' }, content: 'UI/UX Designer' },
                    { elem: 'icons', content: [
                        { block: 'icon', mods: { size: 'xs', color: 'gray', glyph: 'twitter'} },
                        { block: 'icon', mods: { size: 'xs', color: 'gray', glyph: 'facebook'} },
                        { block: 'icon', mods: { size: 'xs', color: 'gray', glyph: 'twitter'} }
                      ]
                    },

                  ]
                }
            },
            {
              elem: 'item',
              elemMods: { background: 'orange', display: 'line'},
              content: {
                elem: 'info-blok',
                content: [
                  {
                    block: 'title',
                    mods: { size: 'ml' },
                    content: 'Ali Sayed'
                  },
                  {
                    block: 'text',
                    mods: { size: 's', padding: 'all-10' },
                    content: 'UI/UX Designer'
                  }
                ]
              }
            }
          ]
        }
      ]
      }]
});
