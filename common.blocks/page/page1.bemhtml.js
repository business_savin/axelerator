block('page').content()(function() {
  return [
    {
    block: 'page',
    title: 'Deviserweb Ltd.',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: 'Описание проекта' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: './index.min.css' },
        { elem: 'css', url: 'https://fonts.googleapis.com/css?family=Open+Sans&display=swap'}
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],
    mods: { theme: 'light' },
    content: 'Страница с тэгом <style>'
}]
})
