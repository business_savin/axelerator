({
  shouldDeps: [
    { block: 'logo' },
    { block: 'text' },
    { block: 'title' },
    { block: 'clear' },
    { block: 'icon', mods: { glyph: 'twetter' } },
    { block: 'icon', mods: { glyph: 'facebook' } },
    { block: 'dropdown' },
    { block: 'popup' },
    { block: 'button' },
    { block: 'row' },
    { block: 'ul' },
    { block: 'li' }
  ]
})
