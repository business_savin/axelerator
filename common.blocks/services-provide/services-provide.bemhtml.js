block('services-provide').content()(function () {
    return [{
      elem: 'wrapp',
      content: [{
        block: 'title',
        mods: { align: 'center', 'text-size': 'h1'},
        content: {
            html: '<h2><span>Services</span> We Provide</h2>'
          }
        },
        {
          block: 'text',
          tag: 'p',
          mods: {cont: 'middle', size:'m', align: 'center'},
          content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit Maiores fuga provident Libero.'
        },
        {
          elem: 'content',
          content:[
            {
              block: 'row',
              content: [
                {
                  elem: 'col',
                  elemMods: { mw: 2 },
                  content: {
                    block: 'ul',
                    tag: 'ul',
                    content: [
                      {
                        elem: 'li',
                        tag: 'li',
                        elemMods: { background: 'color-gray', 'blok-size':'s', display: 'blok', hovered: 'background-color-white', margin: 'bottom', shadow: true, cursor: 'pointer', border:'radius-xs' },
                        content: {
                          block: 'image',
                          mods: { 'icon-position': '1', icon: 'position-absolute'},
                          url: './img/icons-png/4.png',
                          title: 'Иконочка',
                        }
                      },
                      {
                        elem: 'li',
                        tag: 'li',
                        elemMods: { background: 'color-gray', 'blok-size':'s', display: 'blok', hovered: 'background-color-white', margin: 'bottom', shadow: true, cursor: 'pointer', border:'radius-xs' },
                        content: {
                          block: 'image',
                          mods: { 'icon-position': '2', icon: 'position-absolute' },
                          url: './img/icons-png/3.png',
                          title: 'Иконочка',
                        }
                      },
                      {
                        elem: 'li',
                        tag: 'li',
                        elemMods: { background: 'color-gray', 'blok-size':'s', display: 'blok', hovered: 'background-color-white', margin: 'bottom', shadow: true, cursor: 'pointer', border:'radius-xs' },
                        content:{
                          block: 'image',
                          mods: { 'icon-position': '3', icon: 'position-absolute' },
                          url: './img/icons-png/1.png',
                          title: 'Иконочка',
                        }
                      },
                      {
                        elem: 'li',
                        tag: 'li',
                        elemMods: { background: 'color-gray', 'blok-size':'s', display: 'blok', hovered: 'background-color-white', margin: 'bottom', shadow: true, cursor: 'pointer', border:'radius-xs' },
                        content: {
                          block: 'image',
                          mods: { 'icon-position': '4', icon: 'position-absolute' },
                          url: './img/icons-png/2.png',
                          title: 'Иконочка',
                        }
                      }
                    ]
                  }
                },
                {
                  elem: 'col',
                  elemMods: { mw: 6 },
                  content: {
                    elem: 'content_wrap',
                    content: [{
                        block: 'title',
                        content: { html: '<h3>Modern App Design<span> </span></h3>' }
                      },
                      { block: 'text', tag: 'p', mods: { size:'m' }, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus earum totam ad sint error porro. Consectetur, sunt, aperiam. Magni rem optio distinctio illum consequuntur delectus nulla illo aperiam itaque cumque.'},
                      { block: 'text', tag: 'p', mods: { size:'m' }, content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel officia vitae eveniet quam rem unde facilis.'},
                      { block: 'text', tag: 'p', mods: { size:'m' }, content: 'vitae eveniet quam rem unde facilis.'},
                      { block: 'button',
                        mods: { theme: 'snow', margin: 'top-left' },
                        content: [
                          {
                            block: 'icon', mods: { size: 'xs', color: 'white', glyph: 'plus'}
                          },
                          {
                            block: 'text',
                            mods: { color: 'black', padding: 'left', size:'ml' },
                            tag: 'span',
                            content:'Know More'
                          }
                        ]
                      }
                    ]
                  }
                },
                {
                  elem: 'col',
                  elemMods: { mw: 4 },
                  content:
                    {
                      block: 'image',
                      mods: { padding: 'left' },
                      url: './img/iwatch.jpg',
                      title: 'Логотип компании',
                    }
                }
              ]
            },
          ]
        }]
      }]
});
