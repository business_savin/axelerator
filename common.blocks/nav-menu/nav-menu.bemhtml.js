block('nav-menu').content()(function() {
  return [
    {
    block: 'dropdown',
    mods: {
        switcher: 'button',
        theme: 'islands',
        size: 'm'
    },
    switcher: {
        block: 'button',
        mods: {
            togglable: 'check',
            theme: 'islands',
            size: 'm'
        },
        text: 'Узнать об акциях на сайте'
    },
    popup: 'Скидка 30% на новую коллекцию. Для активации акции нужно ввести промокод.'
}

   ]
});
