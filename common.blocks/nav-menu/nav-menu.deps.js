({
  shouldDeps: [
    { block: 'logo'},
    { block: 'menu'},
    { block: 'icon', mods: { glyph: 'navicon' } }, 
    { block: 'dropdown'}
  ]
})
