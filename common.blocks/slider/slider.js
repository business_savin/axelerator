modules.define('slider', [
    'jquery',
    'i-bem-dom'
], function(provide,
    $,
    bemDom
) {
/*
	метод decl объекта BEM указывает для какого блока делается декларация.

	Первым параметром указывается имя блока, далее
    {
        методы экземпляра (динамические методы)
    },
    {
        статические методы
    }

*/
provide(bemDom.declBlock('slider',
{
    onSetMod: { // служебное свойство onSetMod реализует возможность реагировать на появление или исчезновение модификаторов

        js: { // указываем модификатор
            inited: function() { // значение inited (таким образом реализуем что-то  вроде $( document ).ready() ).
				/* инициализация обработчиков событий на кнопках и запуск update */

				/* метод bindTo умеет слушать события на блоке, вложенных в блок элементах и реагировать на них */
				this._domEvents().on('mouseenter', this.onEnter);
                this._domEvents().on('mouseleave', this.onLeave);

				// находим кнопки и на события клик вызываем колбэк.
                this._domEvents('control').on('click', function(e) { // метод this.elem кэшируемый
				// так как кнопок две мы определяем чему равно значение модификатора элемента slide по которому кликнули
          this[e.bemTarget.getMod('slide') === 'right' ? 'next' : 'prev']();
                });

				this.update();

            }
        }
    },

	update: function() {
		var nextIdx = localStorage["saveIdx"];
		if (nextIdx) {
            this._elems('item').get(nextIdx).setMod('state', 'active');
		} else {
            this._elems('item').get(0).setMod('state', 'active');
		}

	},

	onEnter: function() {
		this._elems('control').setMod('status', 'on');
	},

	onLeave: function() {
		this._elems('control').setMod('status', 'off');
	},

    next: function() {
		/* переход к следующему слайду */
		if (this.sliding) return;
		return this.slide('next');
    },

    prev: function() {
		/* переход к предыдущему слайду */
		if (this.sliding) return;
		return this.slide('prev');
    },

    slide: function(type) {
        var active = this.findChildElem({
            elem : 'item',
            modName : 'state',
            modVal : 'active'
        }).domElem, // находим текущий активный элемент
            next = next || active[type](), // находим следующий
            fallback  = type == 'next' ? 'first' : 'last';

        this.sliding = true; // флаг

        next = next.length ? next : this._elem('item').domElem[fallback](); // если следующий не понятный, то позовем обычную jquery функцию и определим следующий item

        if (this._elem('next').hasMod('state', 'active')) return; // перерабатываем если у следующего item есть модификатор state_active

        // if (next.bem({ block : this.__self, elem : 'item' })).hasMod('state', 'active')) return;

        var nextIdx = this._elem('item').domElem.index(next); // определяем индекс следующей картинки 0,1,2,3,4

		localStorage["saveIdx"] = nextIdx; // сохраняем данный индекс

		this
			.delMod(active, 'state')
			.setMod(next, 'state', 'active')
			.sliding = false;

        return this;
    }



}));

});
