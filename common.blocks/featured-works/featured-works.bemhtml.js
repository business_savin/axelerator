block('featured-works').content()(function () {
    return [{
      elem: 'wrapp',
      content: [{
        block: 'title',
        mods: { align: 'center', 'text-size': 'h1'},
        content: {
            html: '<h2><span>Featured</span> Works</h2>'
          }
        },
        {
          block: 'text',
          tag: 'p',
          mods: {cont: 'middle', size:'m', align: 'center'},
          content: 'Lorem ipsum dolor sit amet consectet ur adipiscing elit  Vestibulum bibend um vestibulum.'
        },
        {
          elem: 'items',
          content:[
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} },
            { elem: 'item', elemMods:{background:'orange'} }
          ]
        }]
      }]
});
